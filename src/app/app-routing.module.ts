import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'master',
    pathMatch: 'full'
  },
  { 
    path: 'signin', 
    loadChildren: './signin/signin.module#SignInPageModule' 
  },
  {
    path: 'master',
    loadChildren: './master/master.module#MasterPageModule'
  },
  {
    path: 'detail',
    loadChildren: './detail/detail.module#DetailPageModule'
  },
  {
    path: 'detail/:id',
    loadChildren: './detail/detail.module#DetailPageModule'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'photos',
    loadChildren: './photos/photos.module#PhotosPageModule'
  },
  {
    path: 'missions',
    loadChildren: './missions/missions.module#MissionsPageModule'
  },
  {
    path: 'chat',
    loadChildren: './chat/chat.module#ChatPageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { 
    path: 'register', 
    loadChildren: './register/register.module#RegisterPageModule' 
  },
  { 
    path: 'users', 
    loadChildren: './users/users.module#UsersPageModule' 
  },
  { 
    path: 'user', 
    loadChildren: './users/user/user.module#UserPageModule' 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
