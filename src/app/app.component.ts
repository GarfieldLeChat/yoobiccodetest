import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Sign in',
      url: '/signin',
      icon: 'person'
    },
    {
      title: 'Master List',
      url: '/master',
      icon: 'stats'
    },
    {
      title: 'Dashboard',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Photos',
      url: '/photos',
      icon: 'images'
    },
    {
      title: 'Available Missons',
      url: '/missions',
      icon: 'infinite'
    },
    {
      title: 'Chat',
      url: '/chat',
      icon: 'chatboxes'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'User',
      url: '/user',
      icon: 'person'
    },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
