import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: 'detail.page.html',
  styleUrls: ['detail.page.scss'],
})

export class DetailPage implements OnInit {

  apiData: any = {};
  private slug: string;
  constructor(
    public api: RestApiService,
    public loadingController: LoadingController,
    public route: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {
    this.slug = this.route.snapshot.paramMap.get('id');
    this.getDataFromApi();
  }

  async getDataFromApi() {
    const loading = await this.loadingController.create({
      message: 'Loading'
    });
    await loading.present();
    await this.api.getDataFromApiById(this.slug)
      .subscribe(res => {
        console.log(res);
        this.apiData = res;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  /**
   * Delete:
   * Not Implemented
   * @param id
   */

  async delete(id) {
    const loading = await this.loadingController.create({
      message: 'Deleting'
    });
    await loading.present();
    await this.api.deleteDataFromApi(id)
      .subscribe(res => {
        loading.dismiss();
        this.router.navigate(['/master']);
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

/**
 * Edit:
 * Not implemented
 * @param id 
 */
  edit(id) {
    this.router.navigate(['/edit', JSON.stringify(id)]);
  }

}
