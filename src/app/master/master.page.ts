import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RestApiService } from '../rest-api.service';
@Component({
  selector: 'app-master',
  templateUrl: 'master.page.html',
  styleUrls: ['master.page.scss'],
})

export class MasterPage implements OnInit {

  apiData: any;

  constructor(
    public api: RestApiService,
    public loadingController: LoadingController,
    public router: Router,
    private storage: Storage,
    private navCtrl: NavController) { }

  ngOnInit() {
    this.getDataFromApis();

    this.storage.get('signInShown').then((result) => {
      if(result === null){
        this.storage.set('signInShown', true);
        this.router.navigate(['/home']);
      }
    })
  }

  async getDataFromApis() {
    const loading = await this.loadingController.create({
      message: 'Loading'
    });
    await loading.present();
    await this.api.getDataFromApi()
      .subscribe(res => {
        this.apiData = res;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  showDetail(id) {
    this.router.navigate(['/detail', JSON.stringify(id)]);
  } navigate(item) {
    this.router.navigate(['/master', JSON.stringify(item)]);
  }
}
