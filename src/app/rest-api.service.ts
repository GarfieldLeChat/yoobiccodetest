import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  const apiUrl = "https://swapi.co/api/people/";

  @Injectable({
    providedIn: 'root'
  })

  export class RestApiService {

    constructor(private http: HttpClient) { }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
        console.error('Error occurred:', error.error.message);
        } else {
        console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        return throwError('Something failed. Please refresh');
    }
    private extractData(res: Response) {
        let body = res;
        return body || { };
    }

    // Basic CRUD functions

    getDataFromApi(): Observable<any> {
        return this.http.get(apiUrl, httpOptions).pipe(
        map(this.extractData),
        catchError(this.handleError));
    }
    getDataFromApiById(id: string): Observable<any> {
        const url = `${apiUrl}${id}`;
        return this.http.get(url, httpOptions).pipe(
        map(this.extractData),
        catchError(this.handleError));
    }

    postDataToApi(data): Observable<any> {
        /** 
         * This is not implemented within this app at this time but
         * could be later on.  
         * Will need to amend the ${apiUrl}/add_data 
         * To correctly reflect the actual endpoint
         */
        const url = `${apiUrl}/add_data`;
        return this.http.post(url, data, httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }
    updateDataOnApi(id: string, data): Observable<any> {
        const url = `${apiUrl}/${id}`;
        return this.http.put(url, data, httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }
    deleteDataFromApi(id: string): Observable<{}> {
        const url = `${apiUrl}/${id}`;
        return this.http.delete(url, httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }
}
