import { LoadingController, NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SignInPage implements OnInit {

  constructor(
    public loadingController: LoadingController,
    public router: Router,
    private navCtrl: NavController) { }

  ngOnInit() {
  }
  signInForm() {
    this.router.navigate(['/master']);
  }
   registerUser() {
     this.router.navigate(['/register']);
   }
}
