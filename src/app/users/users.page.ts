import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage {

  constructor(private router: Router) { }

  onLoadUser(name: string) {
    this.router.navigateByUrl(`user/(user:user/${name})`);
  }
}